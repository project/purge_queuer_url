<?php

namespace Drupal\Test\purge_queuer_url\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the TrafficRegistry.
 *
 * @coversDefaultClass \Drupal\purge_queuer_url\TrafficRegistry
 * @group purge_queuer_url
 */
class TrafficRegistryTest extends KernelTestBase {

  /**
   * The traffic registry object.
   *
   * @var \Drupal\purge_queuer_url\TrafficRegistryInterface
   */
  protected $registry;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * List of modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['purge_queuer_url'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('purge_queuer_url', [
      'purge_queuer_url_tag',
      'purge_queuer_url',
    ]);
    $this->registry = \Drupal::service('purge_queuer_url.registry');
    $this->connection = \Drupal::database();
  }

  /**
   * Test that only URLs that should be returned are returned.
   *
   * @covers ::getUrls
   */
  public function testGetUrlsByTags(): void {
    $this->connection->insert('purge_queuer_url_tag')
      ->fields(['tagid', 'tag'])
      ->values([1, 'node:1'])
      ->values([10, 'node:2'])
      ->execute();
    $this->registry->add('https://www.example.com/yes', ['node:1']);
    $this->registry->add('https://www.example.com/no', ['node:2']);

    $urls = $this->registry->getUrls(['node:1']);
    $this->assertEquals(['https://www.example.com/yes'], $urls);
  }

  /**
   * Test that URLs can be excluded when querying the registry.
   *
   * @covers ::getUrls
   */
  public function testExcludeUrls(): void {
    $this->connection->insert('purge_queuer_url_tag')
      ->fields(['tagid', 'tag'])
      ->values([1, 'node:1'])
      ->execute();
    $this->registry->add('https://www.example.com/yes', ['node:1']);
    $this->registry->add('https://www.example.com/no', ['node:1']);

    $exclude = ['https://www.example.com/no'];
    $urls = $this->registry->getUrls(['node:1'], $exclude);
    $this->assertEquals(['https://www.example.com/yes'], $urls);
  }

}
