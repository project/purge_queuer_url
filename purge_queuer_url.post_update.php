<?php

/**
 * @file
 * Post updates for the URL and path queuer module.
 */

/**
 * Fixes the tag_ids by adding a trailing ';'.
 */
function purge_queuer_url_post_update_fix_tagids_3202581(&$sandbox) {
  \Drupal::database()
    ->query(
      "UPDATE purge_queuer_url SET tag_ids = CONCAT(tag_ids, ';')",
      [],
      ['allow_delimiter_in_query' => TRUE]
    );
}
