# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [8.x-1.1] - 2024-02-07

### Changed
- #3434045 Support Drupal 11 and drop Drupal 9 support


## [8.x-1.1-rc1] - 2023-10-26

### Added
- #3395433 Use GitLab CI for automated tests.

### Changed
- #3037842 Add indexes to the schema.
- #2941913 At runtime track URLs that have already been queued and skip those.
- #3289191 Support Drupal 10 and drop Drupal 8 support

### Fixed
- #3202581 URLs queued that should not be queued.


## [8.x-1.0] - 2020-05-29

### Added
- **Improvement:** Added `CHANGELOG.md` to start tracking changes.
- **Improvement:** Added `.gitattributes` to keep certain files out of packages.
- **Improvement:** Added `composer.json` for integration with Composer.

### Changed
- **Important:** This project now requires Drupal 8 to be updated to a recent
  stable version, which is ``8.8.6``. This requirement supports the ongoing
  commitment to stability, quality and functional equivalent on Drupal 8, while
  paving the way for equal Drupal 9 quality with a single codebase.
- **Improvement:** Tests pass and module tested on D9 (D9 readiness).
- **Improvement:** Code quality has been brought up to date (D9 readiness).

### Fixed
- **Fixed:** Reimplemented the ``sql:sanitize`` plugin for modern Drush.
